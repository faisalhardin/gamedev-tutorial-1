extends Area2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var screensize
export var speed = 300

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	screensize = get_viewport_rect().size

func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
	var velocity = 0
	if Input.is_action_pressed("ui_right"):
		velocity = 1
	if Input.is_action_pressed("ui_left"):
		velocity = -1
		
	position.x += velocity * speed * delta
	position.x += clamp(position.x, 0, screensize.x)
